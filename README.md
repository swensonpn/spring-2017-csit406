# CSIT 406
## About
This public workspace has been made available for you to use as a reference in recalling material discussed in class.

## Resources
1. JavaScript [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference) or [W3Schools](http://www.w3schools.com/jsref/)
2. [Node Package Manager](https://docs.npmjs.com/)
3. [NodeJS](https://nodejs.org/dist/latest-v7.x/docs/api/)
4. [Express Framework](http://expressjs.com/)
5. [Jade Template Engine](http://jadelang.net/)
6. [MongoDB](https://docs.mongodb.com/manual/), [Mongoose](http://mongoosejs.com/)
7. [Passport Security](http://passportjs.org/)

## Setting Up Your Cloud9 Workspace with Git and Bitbucket the first time.
1. Create an account at [cloud9.io](https://c9.io/). You will be asked for a credit/debit card, but you will not be charged for the free plan.
2. Create a new workspace
    - Click the Create a new workspace tile
    - Name your new workspace spring-2017-csit406-"your blackboard username"
    - Change the workspace to private
    - Choose blank for the template
    - Click Create workspace
3. Upgrade Node to version 7.3.0 and make default
    ```shell
    $ nvm install 7.3.0
    $ nvm alias default 7.3.0
    ```
4. Add Content to your workspace.
   - Create a new folder called homework1
   - In the new folder create a new file called 'index.html'
5. Initialize your project as a Git repository.  
    ```shell 
    $ git init ./
    ```
6. Create a new file called .gitignore file and copy in the following contents
    ```shell
    # Logs
    logs
    *.log
    npm-debug.log*
    
    # Runtime data
    pids
    *.pid
    *.seed
    *.pid.lock
    
    # Directory for instrumented libs generated by jscoverage/JSCover
    lib-cov
    
    # Coverage directory used by tools like istanbul
    coverage
    
    # nyc test coverage
    .nyc_output
    
    # Grunt intermediate storage (http://gruntjs.com/creating-plugins#storing-task-files)
    .grunt
    
    # node-waf configuration
    .lock-wscript
    
    # Compiled binary addons (http://nodejs.org/api/addons.html)
    build/Release
    
    # Dependency directories
    node_modules
    jspm_packages
    
    # Optional npm cache directory
    .npm
    
    # Optional eslint cache
    .eslintcache
    
    # Optional REPL history
    .node_repl_history
    
    # Output of 'npm pack'
    *.tgz
    
    # Yarn Integrity file
    .yarn-integrity
    ```
7. Add your new file to the repository.
    ```shell
    $ git add homework1/index.html
    ```
8. Commit the work on the new file
    ```shell
    $ git commit -m "[descriptive message goes here]"
    ```
9. Create an account at [bitbucket.org](https://bitbucket.org/)
    - Click on get started and complete the registration form
    - At the top of the screen after logging in click create repository
        - Name your repository the same as your workspace name.
        - Make sure the repository is private
        - Click create repository
    - In your new repository expand the section called 'I have an existing project'
        - Copy each of the three lines of code beginning with git.  You will use them in step 9
10. Link your new bitbucket account to Cloud9
    - Navigate to your account settings. (gear at the top right of your home page)
    - In the left menu select Connected Services
    - Then select Bitbucket.  A popup window should open asking for your bitbucket credentials.
    - Log in.  Your accounts are now linked.
11. Add a remote repository (from step 5) you may be prompted to type in yes and hit enter.
    ```shell
    $ git remote add origin [Your repository location]
    ```
    ```shell
    $ git push -u origin --all
    ```
    ```shell
    $ git push -u origin --tags
    ```
12. Go to bitbucket and look at your code.
13. One last step.  Share with the instructor.
    - On the overview page of your repository select 'Send Invitation'
    - Search for 'swensonpn' and add to the repository.
    - Verify with the instructor your account is visible.

## Submitting homework after setting up your accounts.
1. Stage any new files to be committed. Hint: homework1/* would stage all files contained in a folder called homework1
    ```shell
    $ git add homework1/*
    ```
2. Commit all your changes to the repository
    ```shell
    $ git commit -m "Submitting Homework 1"
    ```
3. Push your changes to bitbucket
    ```shell
    $ git push origin master
    ```