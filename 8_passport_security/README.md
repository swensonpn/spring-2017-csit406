# Setting up MongoDB

## References
1. [Passport](http://passportjs.org/)

## Install Passport
1. Install
    ```shell
    $ cd <your-project-folder>
    $ npm install passport --save
    $ npm install passport-local --save
    ```
2. Import modules into app.js
    ```javascript
    passport = require("passport"),
    LocalStrategy = require("passport-local"),
    User = require("./models/user");
    ```
3. Create a complete route for the login form
    - Add a new get route to app.js 
    - Add a new route action method to users.js
    - Create a new view for the login form
    - Test
4. Add a login post route that points to passport
    ```javascript
    app.post('/login',passport.authenticate('local',{
      successRedirect:'/',          //where we go on successful login
      failureRedirect:'/login',     //where to go when we fail to login
      failureFlash:true             //use flash messages
    }));
    ```
5. Add the passport middle-ware
    ```javascript
    app.use(passport.initialize());
    app.use(passport.session());
    passport.use(new LocalStrategy(function(username,password,done){
      User.findOne({username:username},function(err,doc){
    	  if(err){                                                              //database error
    	    return done(err,false);
    	  }
    	  if(!doc){                                                             //user not found
    	    return done(null,false,{error:'User or password not found.'});
    	  }else{
    				if(doc.password === doc.hashPassword(password)){            //success
    					return done(null,doc);
    				}else{                                                      //bad password
    					return done(null,false,{error:'User or password not found.'});
    				}
    			}
    	});
    }));
    ```
5. Fix models/User.js
    - Move function hashPassword to below UserSchema declaration
    - Expose the function with 
    ```javascript
    UserSchema.methods.hashPassword
    ```
6. Add user serialize and deserialize User
    ```javascript
    passport.serializeUser(function(user, done) {
    		done(null, user);
    });
    	
    passport.deserializeUser(function(user, done) {
    		done(null, user);
    });
    ```
7. Add Logout
    - Create logout link in one of the views
    - Add a logout get route to app.js
    - Create a logout controller action in users.js
    ```javascript
    exports.logout = function(req,res){
        req.logout();
    	res.redirect('/login');
    	
    };
    ```
8. Add middleware test for login
    - In app.js add a call to app.use
    ```javascript
    app.use(function(req,res,next){
      if(req.user === undefined && req.path !== '/login'){
        res.redirect("/login");
      }else{
        next();
      }
    });
    ```
9. Conditionally display menu based on existance of user
    - Add a variable to the response in middleware
    - Add an if statement to the layout based on existance of a user
10. Secure individual routes within your controllers.