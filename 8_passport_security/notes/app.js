var express = require('express'),
    session = require('express-session'),
    bodyParser = require("body-parser"),
    logger = require("express-logger"),
    cookieParser = require("cookie-parser"),
    flash = require("express-flash"),
    mongoose = require("mongoose"),
    socketIO = require("socket.io"),
    index = require("./controllers/index.js"),
    pages = require("./controllers/pages.js"),
    users = require("./controllers/users.js"),
    //Step 2
    passport = require("passport"),
    LocalStrategy = require("passport-local"),
    User = require("./models/user.js");
    
var sockets = [];

var app = express();
var sessionStore = new session.MemoryStore;

//configuratin goes here
app.set('port', process.env.PORT || 3000);
app.set('views','./views');
app.set('view engine', 'pug');

//Middleware
app.use(express.static('./public'));//set a static route
app.use(logger({path:"logfile.txt"}));
app.use(bodyParser.urlencoded({extended:false}));// parse post boddy (.json()) is also available
app.use(cookieParser('secret'));
app.use(session({
  cookie: { maxAge: 60000 },
    store: sessionStore,
    saveUninitialized: true,
    resave: 'true',
    secret: 'secret'
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(function(username,password,done){
  User.findOne({username:username},function(err,doc){
	  if(err){                                                              //database error
      console.log("Login: database error");	  
	    return done(err,false);
	  }
	  if(!doc){                                                             //user not found
      console.log("Login: user not found")	  ;
	    return done(null,false,{error:'User or password not found.'});
	  }else{
				if(doc.password === doc.hashPassword(password)){            //success
          console.log("Login: success");				
					return done(null,doc);
				}else{                                                      //bad password
          console.log("Login: bad password");
					return done(null,false,{error:'User or password not found.'});
				}
			}
	});
}));
//Step 8
app.use(function(req, res, next){
  //Step 9
  res.locals.user = req.user;
  
  if(req.user === undefined && req.path !== "/login" && req.path !== "/users/create"){
    res.redirect("/login");
  }else{
    next();
  }
});
app.use(flash());

//Step 6
passport.serializeUser(function(user, done) {
		done(null, user);
});
	
passport.deserializeUser(function(user, done) {
		done(null, user);
});

//Mongoose API http://mongoosejs.com/docs/api.html
mongoose.connect(process.env.IP + "/local");
mongoose.connection.on('connected',function(){
  console.log("Connected to MongoDB @" + process.env.IP + "/local");
});
mongoose.connection.on("SIGINT",function(){
  console.log('MongoDB disconnected due to Application Exit');
  process.exit(0);
});


//assign routes hear
app.get('/notes', index.home);

app.get('/', pages.index);
app.get('/pages', pages.index);
app.get('/pages/create',pages.createForm);
app.post('/pages/create',pages.createSave);
app.get('/pages/view/:id',pages.view);
app.get('/pages/edit/:id',pages.updateForm);
app.post('/pages/edit/:id',pages.updateSave);
app.get('/pages/delete/:id',pages.delete);

//Step 3
app.get("/login",users.login);
app.get("/logout",users.logout);
app.get('/users', users.index);
app.get('/users/create',users.createForm);
app.post('/users/create',users.createSave);
app.get('/users/view/:id',users.view);
app.get('/users/edit/:id',users.updateForm);
app.post('/users/edit/:id',users.updateSave);
app.get('/users/delete/:id',users.delete);

//Step 4
app.post('/login',passport.authenticate('local',{
  successRedirect:'/',          //where we go on successful login
  failureRedirect:'/login',     //where to go when we fail to login
  failureFlash:true             //use flash messages
}));

//start the application
var server = app.listen(process.env.PORT, function () {
  console.log('Example app listening on port ' + process.env.PORT + '!');
});

var io = socketIO.listen(server);

//listen for connections
io.on('connection',function(socket){
  console.log("We have a new connection with " + socket.id);
  socket.emit('info',{message:"A new connection has been made " + socket.id});
  
  sockets.push({id:socket.id,socket:socket,username:"annonymous"});
  
  socket.on("message", function(data){
    console.log("Received a message from " + this.id);
    this.broadcast.emit("message",data);
  });
  
  socket.on("text-change", function(data){
    console.log("The document changed " + data.change);
    this.broadcast.emit("text-change",data);
  });
  
  socket.on("disconnect",function(){
    console.log("Disconnected " + this.id);
  });
});