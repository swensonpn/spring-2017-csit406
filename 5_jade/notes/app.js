var express = require('express'),
    session = require('express-session'),
    bodyParser = require("body-parser"),
    logger = require("express-logger"),
    cookieParser = require("cookie-parser"),
    flash = require("express-flash"),
    index = require("./controllers/index.js"),
    pages = require("./controllers/pages.js"),
    users = require("./controllers/users.js");
    

var app = express();
var sessionStore = new session.MemoryStore;

//configuratin goes here
app.set('port', process.env.PORT || 3000);
app.set('views','./views');
app.set('view engine', 'pug');

//Middleware
app.use(express.static('./public'));//set a static route
app.use(logger({path:"logfile.txt"}));
app.use(bodyParser.urlencoded({extended:false}));// parse post boddy (.json()) is also available
app.use(cookieParser('secret'));
app.use(session({
  cookie: { maxAge: 60000 },
    store: sessionStore,
    saveUninitialized: true,
    resave: 'true',
    secret: 'secret'
}));
app.use(flash());

//assign routes hear
app.get('/notes', index.home);

app.get('/', pages.index);
app.get('/pages', pages.index);
app.get('/pages/create',pages.createForm);
app.post('/pages/create',pages.createSave);
app.get('/pages/view/:id',pages.view);
app.get('/pages/edit/:id',pages.updateForm);
app.post('/pages/edit/:id',pages.updateSave);
app.get('/pages/delete/:id',pages.delete);

app.get('/users', users.index);
app.get('/users/create',users.createForm);
app.post('/users/create',users.createSave);
app.get('/users/view/:id',users.view);
app.get('/users/edit/:id',users.updateForm);
app.post('/users/edit/:id',users.updateSave);
app.get('/users/delete/:id',users.delete);

//start the application
app.listen(process.env.PORT, function () {
  console.log('Example app listening on port ' + process.env.PORT + '!');
});