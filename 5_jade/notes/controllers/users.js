var User = require('../models/user.js');

exports.index = function(req,res){
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    User.find(function(err,docs){
        if(err){
            flash.error = "There was an error locating your Users";
            docs = [];
        }
        
        res.render("users/index",{
            title:"My Users",
            flash:flash,
            docs:docs
        })
    });
};

exports.createForm = function(req,res){
    res.render("users/create",{
        title:"Create User",
        flash:{},
        doc:{}
    })
};

exports.createSave = function(req,res){
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    let user = new User({
        path:"/",
        author_id:123,
        title:req.body.title,
        text:req.body.text
    });
    
    User.save(function(err){
        if(err){
            req.flash('error','User failed to save');
            res.render("users/create",{
               title:"Create User",
               flash:flash,
               doc:user
            });
        }else{
            req.flash('notice','User saved successfully');
            res.redirect('/Users');
        }
    });
};

exports.view = function(req,res){
    let id = req.params.id || 0;
    
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    User.findById(id, function(err, doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/users');
        }else{
            res.render("users/view",{
                title:"View User",
                flash:flash,
                doc:doc
            });
        }
    });
};

exports.updateForm = function(req,res){
    let id = req.params.id || 0;
    
    User.findById(id, function(err,doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/users');
        }else{
            res.render("users/edit",{
               title:"Edit User",
               flash:{},
               doc:doc
            });
        }
    });
};

exports.updateSave = function(req,res){
    let id = req.params.id || 0;
    
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    User.findById(id, function(err,doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/Users');
        }else{
            doc.title = req.body.title;
            doc.text = req.body.text;
            doc.save(function(err){
                if(err){
                    req.flash('error','User failed to save');
                    res.render("users/edit",{
                        title:"Edit User",
                        flash:flash,
                        doc:doc
                    });
                }else{
                    req.flash('notice','User saved successfully');
                    res.redirect('/users/view/' + id);
                }
            });
        }
    });
};

exports.delete = function(req,res){
    let id = req.params.id || 0;
    
    User.findById(id, function(err,doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/users');
        }else{
            doc.remove(function(err){
                if(err){
                    req.flash('error','Error deleting User');
                    res.redirect('/users');
                }else{
                    req.flash('notice',doc.title + " has been deleted");
                    res.redirect("/users");
                }
            });
        }
    });
    
};