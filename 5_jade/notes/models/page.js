let data = [];



function Page(page){
    this.id = page.id;
    this.path = page.path;
    this.author_id = page.author_id;
    this.title = page.title;
    this.text = page.text;
    
    this.save = function(callback){
        if(this.id === undefined){
            this.id = data.length;
            
            data.push({
                id: this.id,
                path: this.path,
                author_id: this.author_id,
                title: this.title,
                text: this.text
            });
            callback(null);
        }else{
            if(data[this.id] !== undefined){
                data[this.id] = {
                    id: this.id,
                    path: this.path,
                    author_id: this.author_id,
                    title: this.title,
                    text: this.text
                };
                callback(null);
            }else{
                if(typeof callback === "function"){
                    callback("does not exist");
                }
            }
        }
    };
    
    this.remove = function(callback){
        if(data[this.id] !== undefined){
            data.splice(this.id, 1);
            callback(null);
        }else{
            callback("document not found");
        }
    };
    
}

Page.find = function(callback){
    let docs = [];
    
    for(let i=0; i<data.length; i++){
        docs.push(new Page(data[i]));
    }
    
    callback(null, data);
};

Page.findById = function(id, callback){
    if(data[id] !== undefined){
        callback(null, new Page(data[id]));
    }else{
        callback("document not found", null);
    }
};

module.exports = Page;