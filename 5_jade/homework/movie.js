let data = [];



function Movie(movie){
    this.id = movie.id;
    this.title = movie.title;
    this.description = movie.description;
    this.genre = movie.genre;
    this.rating = movie.rating;
    
    this.save = function(callback){
        if(this.id === undefined){
            this.id = data.length;
            
            data.push({
                id: this.id,
                title: this.title,
                description: this.description,
                genre: this.genre,
                rating: this.rating
            });
            callback(null);
        }else{
            if(data[this.id] !== undefined){
                data[this.id] = {
                    id: this.id,
                    title: this.title,
                    description: this.description,
                    genre: this.genre,
                    rating: this.rating
                };
                callback(null);
            }else{
                if(typeof callback === "function"){
                    callback("does not exist");
                }
            }
        }
    };
    
    this.remove = function(callback){
        if(data[this.id] !== undefined){
            data.splice(this.id, 1);
            callback(null);
        }else{
            callback("document not found");
        }
    };
    
}

Movie.find = function(callback){
    let docs = [];
    
    for(let i=0; i<data.length; i++){
        docs.push(new Movie(data[i]));
    }
    
    callback(null, data);
};

Movie.findById = function(id, callback){
    if(data[id] !== undefined){
        callback(null, new Movie(data[id]));
    }else{
        callback("document not found", null);
    }
};

module.exports = Movie;