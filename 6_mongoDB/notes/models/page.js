var mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
    
var PageSchema = new Schema({
    path:{
        type:String
    },
    author_id:{
        type:String
    },
    title:{
        type:String
    },
    text:{
        type:String
    }
});

var Page = mongoose.model('Page',PageSchema);
module.exports = Page;

//built-in validators
/*
unique,
required,
min, max,[6, 'Too few eggs']
enum,
match,
maxlength,
minlength
*/