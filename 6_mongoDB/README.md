# Setting up MongoDB

## References
1. [Cloud 9](https://community.c9.io/t/setting-up-mongodb/1717)
2. [Mongo](https://docs.mongodb.com/manual/reference/mongo-shell/)

## Install MongoDB
1. Get MongoDB Application
    ```shell
    $ sudo apt-get install -y mongodb-org
    ```
2. Install
    ```shell
    $ cd <your-project-folder>
    $ mkdir data
    $ echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
    $ chmod a+x mongod
    ```
3. Running MongoDB
    ```shell
    $ ./mongod
    ```
4. Start your webserver
5. Do your work
6. Stop your webserver
7. Stopping MongoDB
    ```shell
    $ Cntrl + c
    ```
    
## Running Shell Commands
1. Start the Database
2. Open a new Terminal Window (Window > New Terminal)
3. Start Mongo Session
    ```shell
    $ mongo
    ```
4. View dbs
    ```shell
    > show dbs
    ```
4. Switch to local
    ```shell
    > use local
    ```
5. List collections and see data
    ```shell
    > show collections
    > db.<collection name>.find()
    ```
5. Create new collection (cntrl + c if you mess up)
    ```shell
    > coll = db.greetings 
    > coll.insert({greeting:"Hello World"})
    > coll.insert({greeting:"Hello USA"})
    > coll.insert({greeting:"Hello Nebraska"})
    > coll.insert({greeting:"Buffalo County"})
    > coll.insert({greeting:"Hello Kearney"})
    > coll.insert({greeting:"Hello UNK"})
    > coll.insert({greeting:"Hello Otto Olsen"})
    > coll.insert({greeting:"Hello Room 224"})
    ```
6. Query your data
    ```shell
    > coll.find(ObjectId("58bea90f5b2d1ee16b9ce7ac"))
    > coll.find({greeting:"Hello USA"})
    > coll.find({greeting:"Hello USA"},{field1:ObjectId.toString()})
    > coll.find({greeting:{$lt:"Greeting Kearney"}})
    > coll.find({greeting:/Nebraska/})
    ```
6. Update
    ```shell
    > coll.update({greeting:"Hello World"},{greeting:"Hello Mongo World"})
    ```
7. Delete
    ```shell
    > coll.remove(ObjectId("58bea93f5b2d1ee16b9ce7ad"))
    ```
8. Drop Collection
    ```shell
    > coll.drop()
    ```
9. Quit
    ```shell
    > exit
    ```