# Hello World
## Node Package Manager
1. [What is Node Package Manager](https://docs.npmjs.com/getting-started/what-is-npm)
2. Create a package.json file using the command line
   ```shell
   $ mkdir helloworld
   $ cd helloworld
   $ npm init
   ```
   You will be prompted for a bunch of items.  For now only input something for description and author
3. Install the Express framework
    ```shell
    $ npm install express --save
    ```
4. Create a file called app.js in the helloworld folder
5. Add the following code
    ```javascript
    var express = require('express');
    var app = express();
    
    app.get('/', function (req, res) {
      res.send('Hello World!');
    });
    
    app.listen(process.env.PORT, function () {
      console.log('Example app listening on port ' + process.env.PORT + '!');
    });
    ```
6. With app.js as the active editor tab click the run button. This should open a new terminal tab and display "Example app listening on port xxxx!"
7. The new tab in the terminal pane is a run configuration.  Once created the server can be started and stopped from this tab regardless of which file is active in the editor.
8. With the server running click "Preview Running Application" from the preview menu.  This should open a preview web browser tab containing the text "Hello World!".
9. Congratulations you have just created your first Express/NodeJS server-side web application.