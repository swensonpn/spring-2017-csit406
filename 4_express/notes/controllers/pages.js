var Page = require('../models/page.js');

exports.index = function(req,res){
    Page.find(function(err,docs){
        if(err){
            res.send("we have an error");
        }else{
            
            
            let messages = {
                notice:req.flash('notice')[0],
                error:req.flash('error')[0]
            };
           
            if(messages.notice) {
                messages.notice = `<div class="alert alert-success">${messages.notice}</div>`;
            }else{
                messages.notice = '';
            }
            if(messages.error){
               messages.error = `<div class="alert alert-danger">${messages.error}</div>`; 
            }else{
                messages.error = '';
            }
            
            // get our list of items
            let items = "";
            if(docs.length > 0){
                for(let i = 0; i < docs.length; i++){
                    items += `<li>
                                <a href="/pages/view/${docs[i].id}">${docs[i].title}</a>
                            </li>`;
                }
            }else{
                items = `<li>No Documents to Display</li>`;
            }
            
            let html = `
                        <!DOCTYPE html>
                        <html>
                            <head>
                                <link rel="stylesheet" type="text/css" href="css/theme.css"/>
                            </head>
                            <body>
                        <h1>List My Pages</h1>
                        ${messages.notice}
                        ${messages.error}
                        <ul>
                            ${items}
                        </ul>
                        <a href="/pages/create">Create New Page</a>
                        </body>
                        </html>
                    `;
            
            res.send(html);
        }
    });
};

exports.createForm = function(req,res){
    res.send(`
        <h1>Article on Fish</h1>
        <form action="/pages/create" method="POST">
            <div>
                <label for="title">Title</label>
                <input id="title" type="text" name="title" value=""/>
            </div>
            <div>
                <label for="text">Article Text</label>
                <textarea id="text" name="text"></textarea>
            </div>
            <div>
                <input type="submit" name="submit" value="Save"/>
            </div>
        </form>
    `);
    
};

exports.createSave = function(req,res){
    console.log(req.body.title);
    let page = new Page({
        path:"/",
        author_id:123,
        title:req.body.title,
        text:req.body.text
    });
    
    page.save(function(err){
        if(err){
            req.flash('error','Page failed to save');
            res.send("Error Saving");
        }else{
            req.flash('notice','Page saved successfully');
            res.redirect('/pages');
        }
    });
};

exports.view = function(req,res){
    let id = req.params.id || 0;
    
    Page.findById(id, function(err, doc){
        if(err){
            res.send("Document not found");
        }else{
            
            let messages = {
                notice:req.flash('notice')[0],
                error:req.flash('error')[0]
            };
           
            if(messages.notice) {
                messages.notice = `<div class="alert alert-success">${messages.notice}</div>`;
            }else{
                messages.notice = '';
            }
            if(messages.error){
               messages.error = `<div class="alert alert-danger">${messages.error}</div>`; 
            }else{
                messages.error = '';
            }
            
            res.send(`
                    <!DOCTYPE html>
                        <html>
                            <head>
                                <link rel="stylesheet" type="text/css" href="/css/theme.css"/>
                            </head>
                            <body>
                            
                    <h1>${doc.title}</h1>
                    ${messages.notice}
                    ${messages.error}
                    <p>
                        ${doc.text}
                    </p>
                    <a href="/pages/update/${doc.id}">edit</a> 
                    <a href="/pages/delete/${doc.id}" onclick="javascript:if(!confirm("are you sure"){return false;})">delete</a>
                    
                            </body>
                        </html>
                `);
        }
    });
};

exports.updateForm = function(req,res){
    let id = req.params.id || 0;
    
    Page.findById(id, function(err,doc){
        if(err){
            res.send("document not found");
        }else{
            res.send(`
                <h1>Edit Article</h1>
                <form action="/pages/update" method="POST">
                    <input type="hidden" name="id" value="${doc.id}"/>
                    
                    <div>
                        <label for="title">Title</label>
                        <input id="title" type="text" name="title" value="${doc.title}"/>
                    </div>
                    <div>
                        <label for="text">Article Text</label>
                        <textarea id="text" name="text">${doc.text}</textarea>
                    </div>
                    <div>
                        <input type="submit" name="submit" value="Save"/>
                    </div>
                </form>
            `);
        }
    });
};

exports.updateSave = function(req,res){
    let id = req.params.id || 0;
    
    Page.findById(id, function(err,doc){
        if(err){
            res.send("document not found");
        }else{
            doc.title = req.body.title;
            doc.text = req.body.text;
            doc.save(function(err){
                if(err){
                    req.flash('error','Page failed to save');
                    res.send("Error Saving");
                }else{
                    req.flash('notice','Page saved successfully');
                    res.redirect('/pages/view/' + req.body.id);
                }
            });
        }
    });
};

exports.delete = function(req,res){
    let id = req.params.id || 0;
    
    Page.findById(id, function(err,doc){
        if(err){
            res.send('document not found');
        }else{
            doc.remove(function(err){
                if(err){
                    res.send("error deleting page");
                }else{
                    res.redirect("/pages");
                }
            });
        }
    });
    
};