var express = require('express'),
    session = require('express-session'),
    bodyParser = require("body-parser"),
    logger = require("express-logger"),
    cookieParser = require("cookie-parser"),
    flash = require("express-flash"),
    pages = require("./controllers/pages.js");

var app = express();
var sessionStore = new session.MemoryStore;

//configuratin goes here
app.set('port', process.env.PORT || 3000);


//Middleware
app.use(express.static('public'));//set a static route
app.use(logger({path:"logfile.txt"}));
app.use(bodyParser.urlencoded({extended:false}));// parse post boddy (.json()) is also available
app.use(cookieParser('secret'));
app.use(session({
  cookie: { maxAge: 60000 },
    store: sessionStore,
    saveUninitialized: true,
    resave: 'true',
    secret: 'secret'
}));
app.use(flash());

//assign routes hear
app.get('/', function (req, res) { 
    res.send(`
        <h1>Home Page</h1>
        <a href="/pages">Log in</a>
    `);
});

app.get('/pages', pages.index);
app.get('/pages/create',pages.createForm);
app.post('/pages/create',pages.createSave);
app.get('/pages/view/:id',pages.view);
app.get('/pages/update/:id',pages.updateForm);
app.post('/pages/update',pages.updateSave);
app.get('/pages/delete/:id',pages.delete);

//start the application
app.listen(process.env.PORT, function () {
  console.log('Example app listening on port ' + process.env.PORT + '!');
});