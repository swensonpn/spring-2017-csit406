var socket = io.connect("/");

socket.on("info", function(data){
   console.log(data.message); 
});

socket.on("message", function(data){
   console.log(data.message); 
});

function sendMessage(msg){
    socket.emit("message", {message:msg});
}

var content = document.getElementById("text-id");
if(content !== null){
    var editor = new Quill("#editor", {
        theme:"snow"
    });
    
    if(editor.container !== null){
        content.parentNode.style.display = "none";
        
        content.form.addEventListener("submit",function(e){
            content.value = editor.root.innerHTML;
            console.log(content.value);
        },false);
        
        editor.on("text-change", function(delta){
           socket.emit("text-change",{change:delta}); 
        });  
        
        socket.on("text-change",function(data){
            editor.updateContents(data.change,"silent");
        });
        
    }
}