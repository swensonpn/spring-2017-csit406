var Page = require('../models/page.js'),
    validationFuncs = require('../funclibs/validation.js');

exports.index = function(req,res){
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
   
    Page.find({},function(err,docs){
        if(err){
            flash.error = "There was an error locating your pages";
            docs = [];
        }
        
        res.render("pages/index",{
            title:"My Pages",
            flash:flash,
            docs:docs
        })
    });
};

exports.createForm = function(req,res){
    res.render("pages/create",{
        title:"Create Page",
        flash:{},
        doc:{}
    })
};

exports.createSave = function(req,res){
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    let page = new Page({
        path:"/",
        author_id:123,
        title:req.body.title,
        text:req.body.text
    });
    
    page.save(function(err){
        if(err){
            validationFuncs.errorHelper(err,function(errors){
                if(!errors){
                    errors = "Failed to save user";
                }
                res.render("pages/create",{
                   title:"Create Page",
                   flash:{error:errors},
                   doc:page
                });
            });
        }else{
            req.flash('notice','Page saved successfully');
            res.redirect('/pages');
        }
    });
};

exports.view = function(req,res){
    let id = req.params.id || 0;
    
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    Page.findById(id, function(err, doc){
        if(err){
            req.flash('error','Page not found');
            res.redirect('/pages');
        }else{
            res.render("pages/view",{
                title:"View Page",
                flash:flash,
                doc:doc
            });
        }
    });
};

exports.updateForm = function(req,res){
    let id = req.params.id || 0;
    
    Page.findById(id, function(err,doc){
        if(err){
            req.flash('error','Page not found');
            res.redirect('/pages');
        }else{
            res.render("pages/edit",{
               title:"Edit Page",
               flash:{},
               doc:doc
            });
        }
    });
};

exports.updateSave = function(req,res){
    let id = req.params.id || 0;
    
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    Page.findById(id, function(err,doc){
        if(err){
            req.flash('error','Page not found');
            res.redirect('/pages');
        }else{
            doc.title = req.body.title;
            doc.text = req.body.text;
            doc.save(function(err){
                if(err){
                    validationFuncs.errorHelper(err,function(errors){
                        if(!errors){
                            errors = "Failed to save user";
                        }
                        res.render("pages/edit",{
                           title:"Edit Page",
                           flash:{error:errors},
                           doc:doc
                        });
                    });
                }else{
                    req.flash('notice','Page saved successfully');
                    res.redirect('/pages/view/' + id);
                }
            });
        }
    });
};

exports.delete = function(req,res){
    let id = req.params.id || 0;
    
    Page.findById(id, function(err,doc){
        if(err){
            req.flash('error','Page not found');
            res.redirect('/pages');
        }else{
            doc.remove(function(err){
                if(err){
                    req.flash('error','Error deleting page');
                    res.redirect('/pages');
                }else{
                    req.flash('notice',doc.title + " has been deleted");
                    res.redirect("/pages");
                }
            });
        }
    });
    
};