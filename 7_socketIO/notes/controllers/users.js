var User = require('../models/user.js'),
    validationFuncs = require('../funclibs/validation.js');

var permissions = ['author','administrator'];

exports.index = function(req,res){
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    User.find(function(err,docs){
        if(err){
            flash.error = "There was an error locating your Users";
            docs = [];
        }
        
        res.render("users/index",{
            title:"My Users",
            flash:flash,
            docs:docs
        })
    });
};

exports.createForm = function(req,res){
    res.render("users/create",{
        title:"Create User",
        permissions:permissions,
        flash:{},
        doc:{}
    })
};

exports.createSave = function(req,res){
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    let user = new User({
        username:req.body.username,
        password:req.body.password,
        email:req.body.email,
        name:req.body.name,
        permissions:req.body.permissions
    });
   
    
    user.save(function(err){
        if(err){
            validationFuncs.errorHelper(err,function(errors){
                if(!errors){
                    errors = "Failed to save user";
                }
                res.render("users/create",{
                   title:"Create User",
                   permissions:permissions,
                   flash:{error:errors},
                   doc:user
                });
            });
        }else{
            req.flash('notice','User saved successfully');
            res.redirect('/Users');
        }
    });
};

exports.view = function(req,res){
    let id = req.params.id || 0;
    
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    User.findById(id, function(err, doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/users');
        }else{
            res.render("users/view",{
                title:"View User",
                flash:flash,
                permissions:permissions,
                doc:doc
            });
        }
    });
};

exports.updateForm = function(req,res){
    let id = req.params.id || 0;
    
    User.findById(id, function(err,doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/users');
        }else{
            res.render("users/edit",{
               title:"Edit User",
               flash:{},
               permissions:permissions,
               doc:doc
            });
        }
    });
};

exports.updateSave = function(req,res){
    let id = req.params.id || 0;
    
    let flash = {
        notice:req.flash('notice')[0],
        error:req.flash('error')[0]
    }
    
    User.findById(id, function(err,doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/Users');
        }else{
            
            doc.username = req.body.username;
            doc.email = req.body.email;
            doc.name = req.body.name;
            doc.permissions = req.body.permissions;
            doc.updated = Date.now();
            doc.save(function(err){
                if(err){
                    validationFuncs.errorHelper(err,function(errors){
                        if(!errors){
                            errors = "Failed to save user";
                        }
                        res.render("users/edit",{
                           title:"Edit User",
                           permissions:permissions,
                           flash:{error:errors},
                           doc:doc
                        });
                    });
                }else{
                    req.flash('notice','User saved successfully');
                    res.redirect('/users/view/' + id);
                }
            });
        }
    });
};

exports.delete = function(req,res){
    let id = req.params.id || 0;
    
    User.findById(id, function(err,doc){
        if(err){
            req.flash('error','User not found');
            res.redirect('/users');
        }else{
            doc.remove(function(err){
                if(err){
                    req.flash('error','Error deleting User');
                    res.redirect('/users');
                }else{
                    req.flash('notice',doc.title + " has been deleted");
                    res.redirect("/users");
                }
            });
        }
    });
    
};